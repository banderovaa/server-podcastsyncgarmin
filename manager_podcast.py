#! /Library/Frameworks/Python.framework/Versions/3.8/bin/python3.8
# -*- coding: utf-8 -*-

import sqlite3
import re
import os
import requests
from pydub import AudioSegment
import api_listennotes
import json
from datetime import date
from datetime import datetime
import re
from pydub import AudioSegment
import sys  
from  PIL import Image

DATABASE="podcast.db"
URL_FORMAT = '/media/{podcast}_{episode}.mp3'


def createDatabase():
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS Podcast (id INTEGER PRIMARY KEY,id_listennotes TEXT,title TEXT, publisher TEXT, description TEXT, image TEXT, last_check INTEGER DEFAULT 0)''')
    cursor.execute('''CREATE TABLE IF NOT EXISTS Episode (id INTEGER PRIMARY KEY,id_podcast TEXT,id_listennotes TEXT,position INTEGER, title TEXT,description TEXT,url TEXT,pub_date_ms TEXT ,duration INTEGER, downloaded INTEGER DEFAULT 0,readed INTEGER DEFAULT 0)''')
    dataBase.commit
    dataBase.close()

def listPodcastWatch():
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute("SELECT Podcast.id, Podcast.title, COUNT(*) FROM Podcast LEFT JOIN Episode ON Episode.id_podcast = Podcast.id_listennotes WHERE readed = 0 AND downloaded = 1 GROUP BY Podcast.id ORDER BY Podcast.title")
    podcasts = []
    for podcast in cursor.fetchall():
        podcasts.append({
            'id': podcast[0],
            'name': podcast[1],
            "nb_episodes" :podcast[2]
        })
    dataBase.close()
    return podcasts

def listEpisodeWatch(id):
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute("SELECT Episode.id,Podcast.title,Episode.position,Episode.duration from Episode LEFT JOIN Podcast on Episode.id_podcast=Podcast.id_listennotes WHERE readed=0 AND downloaded=1 And Podcast.id=? ORDER BY Episode.position", (id,))
    episodes = []
    for episode in cursor.fetchall():
        episodes.append({
            'url': URL_FORMAT.format(podcast=re.sub('[^A-Za-z0-9]+', '',episode[1]), episode="episode_"+str(episode[2])),
            'id': episode[0],
            'name': "épisode: "+str(episode[2]),
            'duration':episode[3]
        })
    dataBase.close()
    return episodes

def readedEpisodes(ids):
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    for id in ids.split(","):
        cursor.execute("UPDATE Episode SET readed = 1 WHERE id = ?", (id,))
        dataBase.commit()
    dataBase.close()    
    return {}

def notReadedEpisodes(ids):
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    for id in ids.split(","):
        cursor.execute("UPDATE Episode SET readed = 0 WHERE id = ?", (id,))
        dataBase.commit()
    return {}

def addPodcast(id_listennotes):
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute("SELECT * FROM Podcast WHERE id_listennotes = ? ",(id_listennotes,))
    if len( cursor.fetchall() ) == 0:
        cursor.execute("INSERT INTO Podcast (id_listennotes) VALUES (?)",(id_listennotes,))
        dataBase.commit()
    dataBase.close()    
    return {}

def removePodcast(id):
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute("SELECT  id_listennotes FROM Podcast WHERE id = ? ",(id,))
    rows = cursor.fetchall()
    if len(rows) == 1:
        cursor.execute("DELETE FROM Podcast WHERE id_listennotes=? ",(rows[0][0],))
        cursor.execute("DELETE FROM Episode WHERE id_podcast=? ",(rows[0][0],))
        dataBase.commit()
    dataBase.close()    
    return {}

def listPodcast():
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute("SELECT Podcast.id_listennotes, Podcast.last_check from Podcast")
    for podcast in cursor.fetchall():
        today = date.today()
        someday = date.fromtimestamp(podcast[1])
        diff = today - someday 
        
        if(diff.days>=1):
            print("request listennotes podcast "+podcast[0])
            pagination(dataBase,podcast[0],lastPubDateEpisode(dataBase,podcast[0]))
    
    cursor.execute("SELECT Podcast.id,Podcast.title, Podcast.description, Podcast.publisher, Podcast.image,Podcast.id_listennotes from Podcast LEFT JOIN Episode on Episode.id_podcast=Podcast.id_listennotes GROUP BY Podcast.id ORDER BY Podcast.title")
    podcasts = []
    for podcast in cursor.fetchall():
        cursor.execute("SELECT Episode.id,Episode.title,Episode.description,Episode.position,Episode.pub_date_ms, Episode.duration, Episode.url, Episode.downloaded,Episode.readed from Episode WHERE Episode.id_podcast=? ORDER BY Episode.position", (podcast[5],))
        episodes = []
        for episode in cursor.fetchall():
            episodes.append({
                'id':episode[0],
                'title': episode[1],
                'description': episode[2],
                'position': episode[3],
                'pub_date_ms':episode[4],
                'duration': episode[5],
                'url': URL_FORMAT.format(podcast=re.sub('[^A-Za-z0-9]+', '',podcast[1]), episode="episode_"+str(episode[3])),
                'url_external': episode[6],
                'downloaded': episode[7],
                'readed': episode[8],     
            })
        podcasts.append({
            'id': podcast[0],
            'title': podcast[1],
            'description': podcast[2],
            'publisher': podcast[3],
            'image': podcast[4],
            'episodes': episodes
        })
    dataBase.close()    
    return podcasts

def pagination(dataBase,id_podcast,next_episode_pub_date):
    podcast = api_listennotes.getPodcast(id_podcast,next_episode_pub_date)

    cursor = dataBase.cursor()
    now = datetime.now()
    timestamp = datetime.timestamp(now)
    cursor.execute("UPDATE Podcast SET title=?, description=?, publisher=?,image=? , last_check=? WHERE id_listennotes=?",(podcast['title'],podcast["description"],podcast["publisher"],podcast["image"],int(timestamp),id_podcast))
    dataBase.commit()

    for episode in podcast['episodes']:
        if not existEpisode(dataBase,episode['id'],episode['title']):
            position = getPositionEpisode(dataBase,id_podcast)
            cursor.execute("INSERT INTO Episode (id_podcast,id_listennotes,position,title,description,url,pub_date_ms,duration,downloaded,readed) VALUES (?,?,?,?,?,?,?,?,?,?)",(id_podcast,episode['id'],position,episode['title'],episode['description'],episode['audio'],episode['pub_date_ms'],episode['audio_length_sec'],0,0))
            dataBase.commit()

    if podcast['next_episode_pub_date'] is not None and podcast['next_episode_pub_date']!=podcast['latest_pub_date_ms']:
        pagination(dataBase,id_podcast,podcast['next_episode_pub_date'])

def existEpisode(dataBase,id,title):
    cursor = dataBase.cursor()
    cursor.execute("SELECT * FROM Episode WHERE id_listennotes = ? OR title=? ",(id,title))
    return len( cursor.fetchall() ) != 0

def getPositionEpisode(dataBase,id_podcast):
    cursor = dataBase.cursor()
    cursor.execute("SELECT position FROM episode WHERE id_podcast=? ORDER BY pub_date_ms DESC LIMIT 1",(id_podcast,))
    rows = cursor.fetchall()
    if len(rows)==0:
        return 1
    else:
        return rows[0][0]+1

def lastPubDateEpisode(dataBase,id_podcast):
    cursor = dataBase.cursor()
    cursor.execute("SELECT pub_date_ms FROM episode WHERE id_podcast=? ORDER BY pub_date_ms DESC LIMIT 1",(id_podcast,))
    rows = cursor.fetchall()
    if len(rows)==0:
        return 0
    else:
        return rows[0][0]

def removeEpisodeReaded():
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute("SELECT Episode.id,Podcast.title,Episode.title,Episode.position from Episode LEFT JOIN Podcast on Episode.id_podcast=Podcast.id_listennotes WHERE readed=1")
    for episode in cursor.fetchall():
        path = "media/"+re.sub('[^A-Za-z0-9]+', '',episode[1])+"_episode_"+str(episode[3])+".mp3"
        if os.path.exists(path):
            print("Remove episode "+path)
            os.remove(path)
        cursor.execute("UPDATE Episode SET downloaded = 0 WHERE id = ?", (episode[0],))
    dataBase.commit()    
    dataBase.close()
    return {}

def downloadEpisodes(ids):
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    for id in ids.split(","):
        cursor.execute("SELECT Episode.id,Podcast.title,Podcast.image,Episode.title,Episode.position,Episode.url from Episode LEFT JOIN Podcast on Episode.id_podcast=Podcast.id_listennotes WHERE Episode.id=? AND downloaded=0",(id,))
        rows = cursor.fetchall()
        if len(rows)==1:
            episode = rows[0]
            downloadEpisode(episode[1],episode[2],episode[3],episode[4],episode[5])  
            cursor.execute("UPDATE Episode SET downloaded = 1 WHERE id = ?", (episode[0],))
            dataBase.commit() 
    dataBase.close()
    print('End')
    return {}


def downloadPodcast():
    dataBase = sqlite3.connect(DATABASE)
    cursor = dataBase.cursor()
    cursor.execute("SELECT Episode.id,Podcast.title,Podcast.image,Episode.title,Episode.position,Episode.url from Episode LEFT JOIN Podcast on Episode.id_podcast=Podcast.id_listennotes WHERE readed=0 AND downloaded=0")
    for episode in cursor.fetchall():
        downloadEpisode(episode[1],episode[2],episode[3],episode[4], episode[5])
        cursor.execute("UPDATE Episode SET downloaded = 1 WHERE id = ?", (episode[0],))
        dataBase.commit() 

    cursor.execute("SELECT Episode.id,Podcast.title,Episode.title,Episode.position from Episode LEFT JOIN Podcast on Episode.id_podcast=Podcast.id_listennotes WHERE readed=1")
    for episode in cursor.fetchall():
        path = "media/"+re.sub('[^A-Za-z0-9]+', '',episode[1])+"_episode_"+str(episode[3])+".mp3"
        if os.path.exists(path):
            print('Remove=> '+path)
            os.remove(path)
        cursor.execute("UPDATE Episode SET downloaded = 0 WHERE id = ?", (episode[0],))
        dataBase.commit()    
    dataBase.close()    
    print('End')

def downloadEpisode(podcastTitle,podcastImage,episodeTitle,episodePosition,episodeUrl):
    path = "media/"+re.sub('[^A-Za-z0-9]+', '',podcastTitle)+"_episode_"+str(episodePosition)+".mp3"
    if not os.path.exists(path):
        with open('temp', 'wb') as f:
            print('Downloading=> '+podcastTitle+ " épisode: "+episodeTitle)
            response = requests.get(episodeUrl, stream=True)
            total_length = response.headers.get('content-length')

            if total_length is None:  # no content length header
                f.write(response.content)
            else:
                dl = 0
                total_length = int(total_length)
                for data in response.iter_content(chunk_size=4096):
                    dl += len(data)
                    f.write(data)
                    done = int(50 * dl / total_length)
                    sys.stdout.write("\r[%s%s]" % ('=' * done, ' ' * (50-done)) )    
                    sys.stdout.flush()
        mp4_version = AudioSegment.from_file("temp")
        if not os.path.exists("media"):
            os.mkdir("media")
        with open("cover.png", 'wb') as f:
            response = requests.get(podcastImage, stream=True)
            total_length = response.headers.get('content-length')
            if total_length is None:  # no content length header
                f.write(response.content)
            else:
                dl = 0
                total_length = int(total_length)
                for data in response.iter_content(chunk_size=4096):
                    dl += len(data)
                    f.write(data)
                    done = int(50 * dl / total_length)
                    sys.stdout.write("\r[%s%s]" % ('=' * done, ' ' * (50-done)) )    
                    sys.stdout.flush()
        im = Image.open('cover.png')
        im = im.resize((128, 128))  
        im.save('cover.png')          
        print('Converting')
        mp4_version.export("media/"+re.sub('[^A-Za-z0-9]+', '',podcastTitle)+"_episode_"+str(episodePosition)+".mp3",
                        format="mp3",
                        bitrate="128k",
                        tags={'title': 'épisode: '+str(episodePosition)+'', 'artist': ''+podcastTitle+'', 'album': ''+podcastTitle+''},
                        cover="cover.png")
        os.remove("temp")
        os.remove("cover.png")
    

  