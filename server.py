#! /Library/Frameworks/Python.framework/Versions/3.8/bin/python3.8
# -*- coding: utf-8 -*-
from flask import request
from flask import Flask
from flask import send_from_directory
from flask import Response
from flask_caching import Cache
from flaskthreads import ThreadPoolWithAppContextExecutor
import time
from datetime import datetime
import threading
import json
import logging

import manager_podcast
import api_listennotes

config = {
    "DEBUG": True,          # some Flask specific configs
    "CACHE_TYPE": "simple", # Flask-Caching related configs
    "CACHE_DEFAULT_TIMEOUT": 300
}
app = Flask(__name__)
app.config.from_mapping(config)
cache = Cache(app)
manager_podcast.createDatabase()

@app.route('/watch/podcast',methods=[ 'GET'])
@cache.cached(timeout=5)
def get_list_podcast_watch():
    return Response(json.dumps(manager_podcast.listPodcastWatch()),status=200,mimetype='application/json' )

@app.route('/watch/podcast/<int:id_podcast>/episodes',methods=[ 'GET'])
@cache.cached(timeout=5)
def get_list_episode_watch(id_podcast):
    return Response(json.dumps(manager_podcast.listEpisodeWatch(id_podcast)),status=200,mimetype='application/json' )

@app.route('/podcast',methods=[ 'GET'])
@cache.cached(timeout=5)
def get_list_podcast():
    return Response(json.dumps(manager_podcast.listPodcast()),status=200,mimetype='application/json' )

@app.route('/episode/readed',methods=[ 'POST'])
def post_readed_episode():
    ids =request.args.get("ids")
    idsJon = request.json
    
    if( ids is None and idsJon is None):
        return Response(json.dumps({'error':'missing parameter'}),status=400,mimetype='application/json' ) 
    else:
        if( ids is None):
            ids = idsJon.get("ids")
            print("ids : "+ids)
        return Response(json.dumps(manager_podcast.readedEpisodes(ids)),status=200,mimetype='application/json' )

@app.route('/episode/not_readed',methods=[ 'POST'])
def post_not_readed_episode():
    ids =request.args.get("ids")
    if( ids is None):
        return Response(json.dumps({'error':'missing parameter'}),status=400,mimetype='application/json' )
    else:
        return Response(json.dumps(manager_podcast.notReadedEpisodes(ids)),status=200,mimetype='application/json' )

@app.route('/episode/download',methods=[ 'POST'])
def post_download_episode():
    ids =request.args.get("ids")
    if( ids is None):
        return Response(json.dumps({'error':'missing parameter'}),status=400,mimetype='application/json' )
    else:
        threading.Thread( target=download_episode, args=(ids,)).start()
        return Response(json.dumps({}),status=200,mimetype='application/json' )

def download_episode(ids):
    manager_podcast.downloadEpisodes(ids)

@app.route('/podcast/search',methods=[ 'GET'])
@cache.cached(timeout=86400,query_string=True)
def get_search_podcast():
    name =request.args.get("name")
    if( name is None):
        return Response(json.dumps({'error':'missing parameter'}),status=400,mimetype='application/json' )
    else:
        return Response(json.dumps(api_listennotes.searchPodcats(name)),status=200,mimetype='application/json' )

@app.route('/podcast/<id_podcast>/add',methods=[ 'POST'])
def post_add_podcast(id_podcast):
    return Response(json.dumps(manager_podcast.addPodcast(id_podcast)),status=200,mimetype='application/json' )

@app.route('/podcast/<int:id_podcast>/remove',methods=[ 'POST'])
def post_remove_podcast(id_podcast):
    return Response(json.dumps(manager_podcast.removePodcast(id_podcast)),status=200,mimetype='application/json' )

@app.route('/episode/remove',methods=[ 'POST'])
def post_remove_episodes():
    threading.Thread( target=remove_episodes).start()
    return Response(json.dumps({}),status=200,mimetype='application/json' )

def remove_episodes():
    manager_podcast.removeEpisodeReaded()

@app.route('/podcast/download',methods=[ 'POST'])
def post_download_podcast():
    threading.Thread( target=download_podcast).start()
    return Response(json.dumps({}),status=200,mimetype='application/json' )

def download_podcast():
    manager_podcast.downloadPodcast()

@app.route('/media/<path:filename>',methods=[ 'GET'])
def downloadFile (filename):
    return send_from_directory(directory="media", filename=filename)

if __name__ == '__main__':
    #logging.basicConfig(filename='error.log',level=logging.DEBUG)
    app.run(debug=True, host='0.0.0.0')    