# Enter a API key
Request an API key on https://www.listennotes.com/api/

Log in, you can just request a free account, you are entitled to 5000 requests per month.
After you will find your token in your dashboard or at the following address: https://www.listennotes.com/api/dashboard/#apps

In the file api_listennotes.py replace YOUR_KEY with the token that you have just recovered.

# Start Server 
To launch the server you must have python 3 installed

Then execute the following command <code>python server.py</code>

On the Android application and on the Garmin Connect application, modify the url to indicate your server url.

On the Android application it is via the settings button at the top right then change the server url

On the Garmin Connect application it is via Garmin Connect or Garmin Express. There is a url parameter for the application.

If you launch on your computer, it's your IP

# Application 
## Android
https://play.google.com/store/apps/details?id=com.ravenfeld.garmin.podcasts

## Garmin Connect IQ
https://apps.garmin.com/fr-FR/apps/5c22c9d7-4f38-4e03-8897-ad393b705dad

# Question ?
Do not hesitate to send me an email to alexis.lecanu.garmin@gmail.com if you have any concerns
